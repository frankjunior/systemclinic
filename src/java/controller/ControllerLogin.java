/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Dao.DaoUsuario;
import model.bean.BeanUsuario;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerLogin", urlPatterns = {"/ControllerLogin"})
public class ControllerLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerLogin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerLogin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ((HttpServletRequest) request).getSession().invalidate();
        response.sendRedirect("login.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BeanUsuario usuario = new BeanUsuario();
        String login = null;
        String senha = null;
        login = request.getParameter("login");
        senha = request.getParameter("senha");
        System.out.println("controller.ControllerLogin.doPost(): " + login);
        usuario.setUsu_email(login);
        usuario.setUsu_senha(senha);
        try {
            usuario = new DaoUsuario().efetuarLogin(usuario);
            if (usuario.getUsu_email() != null) {
                //if (usuario.getUsu_senha().equals(senha)){
                    
                    System.out.println("Logado com sucesso!");
                    request.getSession().setAttribute("user", usuario.getUsu_email());
                    request.getSession().setAttribute("user_key", usuario.getUsu_cod());
                    request.getSession().setAttribute("username", usuario.getUsu_email().substring(0, usuario.getUsu_email().indexOf("@")));
                    request.getSession().setAttribute("role", usuario.getUsu_tusucod().gettUsu_cod());

                    /* init session */
                    HttpSession session = request.getSession();
                    session.setAttribute("user", usuario.getUsu_email());
                    //setting session to expiry in 30 mins
                    session.setMaxInactiveInterval(30 * 60);
                    Cookie userName = new Cookie("user", usuario.getUsu_email());
                    userName.setMaxAge(30 * 60);
                    response.addCookie(userName);

                    response.sendRedirect("pages/index.jsp");
                //}
            } else {
                System.out.println("Usuário ou senha inválida!");
                response.sendRedirect("login.jsp");
            }

        } catch (Exception e) {
            e.getMessage();
            System.out.println(e);
            response.sendRedirect(request.getContextPath());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

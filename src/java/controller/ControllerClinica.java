/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Dao.DaoBairro;
import model.Dao.DaoClinica;
import model.Dao.DaoEndereco;
import model.Dao.DaoFones;
import model.Dao.DaoSegmento;
import model.Dao.DaoUsuario;
import model.bean.BeanBairro;
import model.bean.BeanClinica;
import model.bean.BeanEndereco;
import model.bean.BeanFones;
import model.bean.BeanSegmento;
import model.bean.BeanTipoUsuario;
import model.bean.BeanUsuario;

/**
 *
 * @author S1PPERRONE
 */
@WebServlet(name = "ControllerClinica", urlPatterns = {"/ControllerClinica"})
public class ControllerClinica extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerClinica</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerClinica at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = null;
        String pathReturn = null;
        if (request.getParameter("page") != null) {
            page = request.getParameter("page");
            switch (page) {
                case ("edtClinica"):
                    BeanUsuario usuario = null;
                    //Buscando o usuário
                    pathReturn = "cadastro";
                    if (request.getParameter("usuario_id") != null) {
                        try {
                            usuario = new DaoUsuario().buscarporcodigo(Integer.parseInt(request.getParameter("usuario_id")));
                        } catch (SQLException ex) {
                            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        System.out.println("Usuário não encontrado.");
                    }

                    //Buscando o endereço
                    BeanEndereco endereco = null;
                    if (usuario != null) {
                        try {
                            endereco = new DaoEndereco().buscarPorCodigo(Integer.parseInt(request.getParameter("bairro")));
                        } catch (SQLException ex) {
                            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        System.out.println("Endereço não encontrado.");
                    }

                    BeanClinica clinica = null;
                    //Buscando a clínica
                    if (request.getParameter("clinica_id") != null) {
                        try {
                            clinica = new DaoClinica().buscarPorCodigo(Integer.parseInt(request.getParameter("clinica_id")));
                        } catch (SQLException ex) {
                            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        System.out.println("Clínica não encontrada.");
                    }

                    BeanFones fone = null;
                    //Buscando fone
                    if (request.getParameter("fone_id") != null) {
                        try {
                            fone = new DaoFones().buscarPorClinica(Integer.parseInt(request.getParameter("fone_id")));
                        } catch (SQLException ex) {
                            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        System.out.println("Fone não localizado.");
                    }

                    BeanSegmento segmento = null;
                    //Buscando segmento
                    if (clinica != null) {
                        try {
                            segmento = new DaoSegmento().buscarPorCodigo(Integer.parseInt(request.getParameter("segmento")));
                        } catch (SQLException ex) {
                            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        System.out.println("Segmento não encontrado.");
                    }

                    BeanBairro bairro = null;
                    //Buscando bairro
                    if (usuario != null) {
                        try {
                            bairro = new DaoBairro().buscarCodigo(Integer.parseInt(request.getParameter("bairro")));
                        } catch (SQLException ex) {
                            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        System.out.println("Bairro não encontrado.");
                    }

                    //atualizando objetos
                    clinica.setClin_razao(request.getParameter("razao"));
                    clinica.setClin_segcod(segmento);
                    endereco.setEnd_baicod(bairro);
                    endereco.setEnd_cep(request.getParameter("cep"));
                    endereco.setEnd_complemento(request.getParameter("complemento"));
                    endereco.setEnd_ref(request.getParameter("ref"));
                    endereco.setEnd_rua(request.getParameter("rua"));
                    usuario.setUsu_endcod(endereco);
                    usuario.setUsu_email(request.getParameter("email"));
                    usuario.setUsu_senha(request.getParameter("pass"));
                    fone.setFon_clincod(clinica);
                    fone.setFon_num1(request.getParameter("telefone1"));
                    fone.setFon_num2(request.getParameter("telefone2"));

                    //altera clinica
                    try {
                        new DaoClinica().salvar(clinica, true);
                    } catch (SQLException ex) {
                        Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //altera endereço
                    try {
                        new DaoEndereco().salvarEnd(endereco);
                    } catch (SQLException ex) {
                        Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //altera usuario
                    try {
                        new DaoUsuario().salvar(usuario);
                    } catch (SQLException ex) {
                        Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //altera fone
                    try {
                        new DaoFones().salvarFone(fone);
                    } catch (SQLException ex) {
                        Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    request.getSession().setAttribute("user", usuario.getUsu_email());
                    break;
                case ("buscarClinica"):
                    pathReturn = "pages";
                    if (request.getParameter("usuario_id") != null) {
                        try {
                            BeanUsuario user = new DaoUsuario().buscarporcodigo(Integer.parseInt(request.getParameter("usuario_id")));
                            System.out.println("BairroCodigo: "+user.getUsu_endcod().getEnd_cod());
                            user.setUsu_status(request.getParameter("status"));
                            new DaoUsuario().salvar(user);
                        } catch (SQLException ex) {
                            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;

                default:
                    System.out.println("Erro na origem da requisição.");
                    break;
            }
        }

        response.sendRedirect("sucess.jsp?title=" + request.getParameter("razao") + "&msg=registro alterado com sucesso&redirect=" + pathReturn + "/" + page + ".jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        BeanBairro bairro = new BeanBairro();
        BeanTipoUsuario tipoUsu = new BeanTipoUsuario();
        BeanUsuario usuario = new BeanUsuario();
        BeanSegmento segmento = new BeanSegmento();
        BeanEndereco end = new BeanEndereco();
        BeanClinica clinica = new BeanClinica();
        BeanFones fone = new BeanFones();
        DaoFones daoFone = new DaoFones();
        DaoClinica daoClinica = new DaoClinica();
        DaoSegmento daoSeg = new DaoSegmento();
        DaoUsuario daoUsuario = new DaoUsuario();
        DaoBairro daoBairro = new DaoBairro();
        DaoEndereco daoEnd = new DaoEndereco();

        bairro.setBai_desc(request.getParameter("bairro"));

        try {
            bairro = daoBairro.buscarporDescricao(bairro.getBai_desc()); // buscar código do bairro selecionado           
        } catch (SQLException ex) {
            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
        }
        // pegar os parametros para salvar o endereço
        end.setEnd_cep(request.getParameter("cep"));
        end.setEnd_rua(request.getParameter("rua"));
        end.setEnd_complemento(request.getParameter("complemento"));
        end.setEnd_ref(request.getParameter("ref"));
        end.setEnd_baicod(bairro);

        try {
            end = daoEnd.salvarEnd(end); // chamar método de salvar endereço e retornando o código do endereço salvo
        } catch (SQLException ex) {
            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
        }

        // salvar usuário
        tipoUsu.settUsu_cod(2); //setando o tipo clinica

        System.out.println("cod endereço" + end.getEnd_cod());

        // setando valores dentro do objeto usuario
        usuario.setUsu_email(request.getParameter("email"));
        usuario.setUsu_senha(request.getParameter("pass"));
        usuario.setUsu_endcod(end);
        usuario.setUsu_tusucod(tipoUsu);
        usuario.setUsu_status("Pendente");
        try {
            usuario = daoUsuario.salvar(usuario); //chamando método salvar usuário e retornando o codigo do usuário salvo
        } catch (SQLException ex) {
            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
        }

        // salvar clínica
        String segDesc = (request.getParameter("segmento"));
        try {
            segmento = daoSeg.buscarporDescricao(segDesc); // buscando codigo do segmento pela descricao
        } catch (SQLException ex) {
            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
        }
        //setando valores no objeto clinica
        clinica.setClin_cnpj(request.getParameter("cnpj"));
        clinica.setClin_razao(request.getParameter("razao"));
        clinica.setClin_segcod(segmento);
        clinica.setClin_usucod(usuario);
        try {
            daoClinica.salvar(clinica, false); // chamando método de salvar clinica
            clinica = daoClinica.buscarPorCnpj(clinica.getClin_cnpj()); // busco a empresa salva
        } catch (SQLException ex) { 
            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
        }

        //salvar telefones clinica
        fone.setFon_clincod(clinica);
        fone.setFon_num1(request.getParameter("telefone1"));
        fone.setFon_num2(request.getParameter("telefone2"));
        try {
            daoFone.salvarFone(fone); // salvando tefones
        } catch (SQLException ex) {
            Logger.getLogger(ControllerClinica.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("sucess.jsp?title=" + request.getParameter("razao") + "&msg=registro cadastrado com sucesso&redirect=login.jsp");

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp); //To change body of generated methods, choose Tools | Templates.

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

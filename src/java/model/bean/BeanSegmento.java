/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author S1PPERRONE
 */
public class BeanSegmento {
    private int seg_cod;
    private String seg_desc;

    public int getSeg_cod() {
        return seg_cod;
    }

    public void setSeg_cod(int seg_cod) {
        this.seg_cod = seg_cod;
    }

    public String getSeg_desc() {
        return seg_desc;
    }

    public void setSeg_desc(String seg_desc) {
        this.seg_desc = seg_desc;
    }
}

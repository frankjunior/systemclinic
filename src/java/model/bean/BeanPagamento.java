/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author Perrone
 */
public class BeanPagamento {
    private int pag_cod;
    private String pag_desc;

    public int getPag_cod() {
        return pag_cod;
    }

    public void setPag_cod(int pag_cod) {
        this.pag_cod = pag_cod;
    }

    public String getPag_desc() {
        return pag_desc;
    }

    public void setPag_desc(String pag_desc) {
        this.pag_desc = pag_desc;
    }
    
}

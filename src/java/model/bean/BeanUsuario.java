/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author S1PPERRONE
 */
public class BeanUsuario {
    
    private int usu_cod;
    private String usu_senha;
    private String usu_email;
    private BeanEndereco usu_endcod;
    private BeanTipoUsuario usu_tusucod;
    private String usu_status;

    public String getUsu_status() {
        return usu_status;
    }

    public void setUsu_status(String usu_status) {
        this.usu_status = usu_status;
    }

    public BeanTipoUsuario getUsu_tusucod() {
        return usu_tusucod;
    }

    public void setUsu_tusucod(BeanTipoUsuario usu_tusucod) {
        this.usu_tusucod = usu_tusucod;
    }

    public int getUsu_cod() {
        return usu_cod;
    }

    public void setUsu_cod(int usu_cod) {
        this.usu_cod = usu_cod;
    }

    public String getUsu_senha() {
        return usu_senha;
    }

    public void setUsu_senha(String usu_senha) {
        this.usu_senha = usu_senha;
    }

    public String getUsu_email() {
        return usu_email;
    }

    public void setUsu_email(String usu_email) {
        this.usu_email = usu_email;
    }

    public BeanEndereco getUsu_endcod() {
        return usu_endcod;
    }

    public void setUsu_endcod(BeanEndereco usu_endcod) {
        this.usu_endcod = usu_endcod;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.sql.Date;

/**
 *
 * @author Perrone
 */
public class BeanCarrinho {
    private int carrinho_cod;
    private String carrinho_status;   
    private BeanPaciente carrinho_paccod;

    public int getCarrinho_cod() {
        return carrinho_cod;
    }

    public void setCarrinho_cod(int carrinho_cod) {
        this.carrinho_cod = carrinho_cod;
    }

    public String getCarrinho_status() {
        return carrinho_status;
    }

    public void setCarrinho_status(String carrinho_status) {
        this.carrinho_status = carrinho_status;
    }   

    public BeanPaciente getCarrinho_paccod() {
        return carrinho_paccod;
    }

    public void setCarrinho_paccod(BeanPaciente carrinho_paccod) {
        this.carrinho_paccod = carrinho_paccod;
    }

    public BeanPagamento getCarrinho_pagcod() {
        return carrinho_pagcod;
    }

    public void setCarrinho_pagcod(BeanPagamento carrinho_pagcod) {
        this.carrinho_pagcod = carrinho_pagcod;
    }
    private BeanPagamento carrinho_pagcod;
}

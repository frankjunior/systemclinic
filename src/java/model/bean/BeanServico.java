/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author Perrone
 */
public class BeanServico {
    private int serv_cod;
    private String serv_desc;

    public int getServ_cod() {
        return serv_cod;
    }

    public void setServ_cod(int serv_cod) {
        this.serv_cod = serv_cod;
    }

    public String getServ_desc() {
        return serv_desc;
    }

    public void setServ_desc(String serv_desc) {
        this.serv_desc = serv_desc;
    }
}

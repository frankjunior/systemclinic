/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author S1PPERRONE
 */
public class BeanEndereco {
    private int end_cod;
    private String end_cep;
    private String end_rua;
    private String end_complemento;
    private String end_ref;
    private BeanBairro end_baicod;

    public int getEnd_cod() {
        return end_cod;
    }

    public void setEnd_cod(int end_cod) {
        this.end_cod = end_cod;
    }
    
    public String getEnd_cep() {
        return end_cep;
    }

    public void setEnd_cep(String end_cep) {
        this.end_cep = end_cep;
    }

    public String getEnd_rua() {
        return end_rua;
    }

    public void setEnd_rua(String end_rua) {
        this.end_rua = end_rua;
    }

    public String getEnd_complemento() {
        return end_complemento;
    }

    public void setEnd_complemento(String end_complemento) {
        this.end_complemento = end_complemento;
    }

    public String getEnd_ref() {
        return end_ref;
    }

    public void setEnd_ref(String end_ref) {
        this.end_ref = end_ref;
    }

    public BeanBairro getEnd_baicod() {
        return end_baicod;
    }

    public void setEnd_baicod(BeanBairro end_baicod) {
        this.end_baicod = end_baicod;
    }
  
    
}

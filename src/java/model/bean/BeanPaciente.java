/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.sql.Date;

/**
 *
 * @author S1PPERRONE
 */
public class BeanPaciente {
    private int pac_cod;
    private Date pac_dtnasc;
    private String pac_tel;
    private String pac_sexo;
    private String pac_cpf;
    private BeanUsuario pac_usucod;

    public int getPac_cod() {
        return pac_cod;
    }

    public void setPac_cod(int pac_cod) {
        this.pac_cod = pac_cod;
    }

    public Date getPac_dtnasc() {
        return pac_dtnasc;
    }

    public void setPac_dtnasc(Date pac_dtnasc) {
        this.pac_dtnasc = pac_dtnasc;
    }

    public String getPac_tel() {
        return pac_tel;
    }

    public void setPac_tel(String pac_tel) {
        this.pac_tel = pac_tel;
    }

    public String getPac_sexo() {
        return pac_sexo;
    }

    public void setPac_sexo(String pac_sexo) {
        this.pac_sexo = pac_sexo;
    }

    public String getPac_cpf() {
        return pac_cpf;
    }

    public void setPac_cpf(String pac_cpf) {
        this.pac_cpf = pac_cpf;
    }

    public BeanUsuario getPac_usucod() {
        return pac_usucod;
    }

    public void setPac_usucod(BeanUsuario pac_usucod) {
        this.pac_usucod = pac_usucod;
    }
    
    
}

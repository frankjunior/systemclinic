/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author Perrone
 */
public class BeanOferece {
    private int ofe_cod;
    private Double ofe_valor;
    private String ofe_status;

    public int getOfe_cod() {
        return ofe_cod;
    }

    public void setOfe_cod(int ofe_cod) {
        this.ofe_cod = ofe_cod;
    }

    public Double getOfe_valor() {
        return ofe_valor;
    }

    public void setOfe_valor(Double ofe_valor) {
        this.ofe_valor = ofe_valor;
    }

    public String getOfe_status() {
        return ofe_status;
    }

    public void setOfe_status(String ofe_status) {
        this.ofe_status = ofe_status;
    }

    public BeanServico getOfe_servcod() {
        return ofe_servcod;
    }

    public void setOfe_servcod(BeanServico ofe_servcod) {
        this.ofe_servcod = ofe_servcod;
    }

    public BeanClinica getOfe_clincod() {
        return ofe_clincod;
    }

    public void setOfe_clincod(BeanClinica ofe_clincod) {
        this.ofe_clincod = ofe_clincod;
    }
    private BeanServico ofe_servcod;
    private BeanClinica ofe_clincod;
}

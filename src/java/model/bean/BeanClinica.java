/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author S1PPERRONE
 */
public class BeanClinica {
    private int clin_cod;
    private String clin_razao;
    private String clin_cnpj;
    private BeanSegmento clin_segcod;
    private BeanUsuario clin_usucod;

    public int getClin_cod() {
        return clin_cod;
    }

    public void setClin_cod(int clin_cod) {
        this.clin_cod = clin_cod;
    }

    public String getClin_razao() {
        return clin_razao;
    }

    public void setClin_razao(String clin_razao) {
        this.clin_razao = clin_razao;
    }

    public String getClin_cnpj() {
        return clin_cnpj;
    }

    public void setClin_cnpj(String clin_cnpj) {
        this.clin_cnpj = clin_cnpj;
    }

    public BeanSegmento getClin_segcod() {
        return clin_segcod;
    }

    public void setClin_segcod(BeanSegmento clin_segcod) {
        this.clin_segcod = clin_segcod;
    }

    public BeanUsuario getClin_usucod() {
        return clin_usucod;
    }

    public void setClin_usucod(BeanUsuario clin_usucod) {
        this.clin_usucod = clin_usucod;
    }
    
    
    
}

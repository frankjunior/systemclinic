/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author S1PPERRONE
 */
public class BeanTipoUsuario {
    
    private int tUsu_cod;
    private String tUsu_descricao;

    public int gettUsu_cod() {
        return tUsu_cod;
    }

    public void settUsu_cod(int tUsu_cod) {
        this.tUsu_cod = tUsu_cod;
    }

    public String gettUsu_descricao() {
        return tUsu_descricao;
    }

    public void settUsu_descricao(String tUsu_descricao) {
        this.tUsu_descricao = tUsu_descricao;
    }
    
}

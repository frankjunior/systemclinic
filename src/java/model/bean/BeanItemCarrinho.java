/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.sql.Date;

/**
 *
 * @author Perrone
 */
public class BeanItemCarrinho {
    private int icarrinho_cod;
    private Date icarrinho_dtAgendamento;
    private Date icarrinho_dtRealizacao;
    private BeanOferece icarrinho_ofecod;
    private BeanCarrinho icarrinho_carrinhoCod;

    public int getIcarrinho_cod() {
        return icarrinho_cod;
    }

    public void setIcarrinho_cod(int icarrinho_cod) {
        this.icarrinho_cod = icarrinho_cod;
    }

    public Date getIcarrinho_dtAgendamento() {
        return icarrinho_dtAgendamento;
    }

    public void setIcarrinho_dtAgendamento(Date icarrinho_dtAgendamento) {
        this.icarrinho_dtAgendamento = icarrinho_dtAgendamento;
    }

    public Date getIcarrinho_dtRealizacao() {
        return icarrinho_dtRealizacao;
    }

    public void setIcarrinho_dtRealizacao(Date icarrinho_dtRealizacao) {
        this.icarrinho_dtRealizacao = icarrinho_dtRealizacao;
    }

    public BeanOferece getIcarrinho_ofecod() {
        return icarrinho_ofecod;
    }

    public void setIcarrinho_ofecod(BeanOferece icarrinho_ofecod) {
        this.icarrinho_ofecod = icarrinho_ofecod;
    }

    public BeanCarrinho getIcarrinho_carrinhoCod() {
        return icarrinho_carrinhoCod;
    }

    public void setIcarrinho_carrinhoCod(BeanCarrinho icarrinho_carrinhoCod) {
        this.icarrinho_carrinhoCod = icarrinho_carrinhoCod;
    }
    
}

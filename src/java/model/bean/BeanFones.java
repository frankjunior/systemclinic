/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author S1PPERRONE
 */
public class BeanFones {
    private int fon_cod;
    private String fon_num1;
    private String fon_num2;
    private BeanClinica fon_clincod;

    public String getFon_num2() {
        return fon_num2;
    }

    public void setFon_num2(String fon_num2) {
        this.fon_num2 = fon_num2;
    }

    public int getFon_cod() {
        return fon_cod;
    }

    public void setFon_cod(int fon_cod) {
        this.fon_cod = fon_cod;
    }

    public String getFon_num1() {
        return fon_num1;
    }

    public void setFon_num1(String fon_num1) {
        this.fon_num1 = fon_num1;
    }

    public BeanClinica getFon_clincod() {
        return fon_clincod;
    }

    public void setFon_clincod(BeanClinica fon_clincod) {
        this.fon_clincod = fon_clincod;
    }
}

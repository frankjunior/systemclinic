/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.sql.Date;

/**
 *
 * @author Perrone
 */
public class BeanMensalidade {
    private int mensal_cod;
    private double mensal_valor;
    private Date mensal_dtVencimento;
    private Date mensal_dtPagamento;
    private String mensal_status;
    private BeanClinica mensal_clincod;
    private BeanPagamento mensal_pagcod;

    public int getMensal_cod() {
        return mensal_cod;
    }

    public void setMensal_cod(int mensal_cod) {
        this.mensal_cod = mensal_cod;
    }

    public double getMensal_valor() {
        return mensal_valor;
    }

    public void setMensal_valor(double mensal_valor) {
        this.mensal_valor = mensal_valor;
    }

    public Date getMensal_dtVencimento() {
        return mensal_dtVencimento;
    }

    public void setMensal_dtVencimento(Date mensal_dtVencimento) {
        this.mensal_dtVencimento = mensal_dtVencimento;
    }

    public Date getMensal_dtPagamento() {
        return mensal_dtPagamento;
    }

    public void setMensal_dtPagamento(Date mensal_dtPagamento) {
        this.mensal_dtPagamento = mensal_dtPagamento;
    }

    public String getMensal_status() {
        return mensal_status;
    }

    public void setMensal_status(String mensal_status) {
        this.mensal_status = mensal_status;
    }

    public BeanClinica getMensal_clincod() {
        return mensal_clincod;
    }

    public void setMensal_clincod(BeanClinica mensal_clincod) {
        this.mensal_clincod = mensal_clincod;
    }

    public BeanPagamento getMensal_pagcod() {
        return mensal_pagcod;
    }

    public void setMensal_pagcod(BeanPagamento mensal_pagcod) {
        this.mensal_pagcod = mensal_pagcod;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author S1PPERRONE
 */
public class BeanBairro {
    
    private int bai_cod;
    private String bai_desc;

    public int getBai_cod() {
        return bai_cod;
    }

    public void setBai_cod(int bai_cod) {
        this.bai_cod = bai_cod;
    }

    public String getBai_desc() {
        return bai_desc;
    }

    public void setBai_desc(String bai_desc) {
        this.bai_desc = bai_desc;
    }
    
}

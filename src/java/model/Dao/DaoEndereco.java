/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Dao.BaseDao.getConnection;
import model.bean.BeanEndereco;

/**
 *
 * @author S1PPERRONE
 */
public class DaoEndereco {
    
    BeanEndereco end = new BeanEndereco();
    PreparedStatement pstm;
    ResultSet rs;
    Connection con;

    public BeanEndereco buscarCep(int cep) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from endereco where end_cep = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, cep);
            rs = pstm.executeQuery();
            end = criarEndereco(rs);
            return end;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Endereço não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public BeanEndereco buscarPorCodigo(int codigo) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from endereco where end_cod = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            end = criarEndereco(rs);
            return end;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Endereço não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private BeanEndereco criarEndereco(ResultSet r) {
        try {
            while (r.next()) {
                end.setEnd_cod(r.getInt("end_cod"));
                end.setEnd_cep(r.getString("end_cep"));
                end.setEnd_rua(r.getString("end_rua"));
                end.setEnd_complemento(r.getString("end_complemento"));
                end.setEnd_ref(r.getString("end_ref"));                
                end.setEnd_baicod(new DaoBairro().buscarCodigo(Integer.parseInt(rs.getString("end_baicod"))));
            } 
            return end;
        } catch (SQLException ex) {
            Logger.getLogger(DaoEndereco.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public BeanEndereco salvarEnd(BeanEndereco end) throws SQLException {
        con = BaseDao.getConnection();
        int codEnd =0;
        try {
            if (end.getEnd_cod()== 0) {
                System.out.println("INSERT Endereco");
                String sql = "insert into endereco(end_cep, end_rua, end_complemento,end_ref, end_baicod) values (?,?,?,?,?)";
                pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                System.out.println("UPDATE Endereco");
                pstm = con.prepareStatement("update endereco set end_cep=?, end_rua=?, end_complemento=?, end_ref = ?, end_baicod = ? where end_cod = ?");
            }
          
           System.out.println(end.getEnd_cep());
           System.out.println(end.getEnd_rua());
           System.out.println(end.getEnd_complemento());
           System.out.println(end.getEnd_ref());
           System.out.println(end.getEnd_baicod().getBai_cod());
          
           
            pstm.setString(1, end.getEnd_cep());
            pstm.setString(2, end.getEnd_rua());
            pstm.setString(3, end.getEnd_complemento());
            pstm.setString(4, end.getEnd_ref());
            pstm.setInt(5, end.getEnd_baicod().getBai_cod());            
            
            if (end.getEnd_cod()> 0)//update
            {
                pstm.setInt(6, end.getEnd_cod());
            }

            int count = pstm.executeUpdate();

            if (count == 0) {
                throw new SQLException("Erro ao salvar enredeço");
            }

            if (end.getEnd_cod()== 0) {
                end.setEnd_cod(getGeneratedId(pstm));
            }
        
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
        return end;
    }
    
    public static int getGeneratedId(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            return id;
        }
        return 0;
    }
}

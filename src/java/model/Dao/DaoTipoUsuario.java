/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Connection.Connection.getConnection;
import model.bean.BeanTipoUsuario;

/**
 *
 * @author S1PPERRONE
 */
public class DaoTipoUsuario {
    
    Connection con;
    PreparedStatement pstm;
    ResultSet rs;
    BeanTipoUsuario tipo = new BeanTipoUsuario();
    
    public BeanTipoUsuario buscarCodigo(int codigo) throws SQLException{      
        con = getConnection();
        try{                    
            String sql="select * from tipousuario where tUsu_cod = ?";            
            pstm = con.prepareStatement(sql);           
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            tipo = createTipoUsu(rs);
            return tipo;                     
        }catch(SQLException | HeadlessException erro){
            System.out.println("Tipo não cadastrada" +erro.getMessage());            
            return null;
        } finally{
             if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
         }       
    }        
     
     private BeanTipoUsuario createTipoUsu(ResultSet r){
        try {
            while (rs.next()) {
                tipo.settUsu_cod(rs.getInt("tUsu_cod"));
                tipo.settUsu_descricao(rs.getString("tUsu_descricao")); 
            }
            return tipo;
        } catch (SQLException ex) {
            Logger.getLogger(DaoTipoUsuario.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
     }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Dao.BaseDao.getConnection;
import model.bean.BeanUsuario;

/**
 *
 * @author S1PPERRONE
 */
public class DaoUsuario extends BaseDao{
    
    Connection con;
    PreparedStatement pstm;
    ResultSet rs;
    BeanUsuario usuario = new BeanUsuario();

    public BeanUsuario buscarUsuario(BeanUsuario usu) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from usuario inner join tipousuario on usu_tipocod = tUsu_cod where usu_email = ?";
            //usu.setUsu_status("Ativo");
            pstm = con.prepareStatement(sql);
            //pstm.setString(1, usu.getUsu_status());
            pstm.setString(1, usu.getUsu_email());
            rs = pstm.executeQuery();
            usuario = criarUsuario(rs);
            return usuario;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Usuario não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public BeanUsuario efetuarLogin(BeanUsuario usu) throws SQLException {
        con = getConnection();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select usu_cod, usu_email, usu_endcod, usu_senha, usu_status, usu_tipocod, tUsu_cod, tUsu_descricao ");
            sb.append("from usuario inner join tipousuario on usu_tipocod = tUsu_cod where usu_email = ? and usu_senha = MD5(?)");
            pstm = con.prepareStatement(sb.toString());
            pstm.setString(1, usu.getUsu_email());
            pstm.setString(2, usu.getUsu_senha());
            rs = pstm.executeQuery();
            usuario = criarUsuario(rs);
            return usuario;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Usuario não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private BeanUsuario criarUsuario(ResultSet r) {
        try {
            while (rs.next()) {
                usuario.setUsu_cod(rs.getInt("usu_cod"));
                usuario.setUsu_email(rs.getString("usu_email"));
                usuario.setUsu_senha(rs.getString("usu_senha"));
                usuario.setUsu_status(rs.getString("usu_status"));
                usuario.setUsu_endcod(new DaoEndereco().buscarPorCodigo(rs.getInt("usu_endcod")));
                usuario.setUsu_tusucod(new DaoTipoUsuario().buscarCodigo(rs.getInt("usu_tipocod")));
            }
            return usuario;
        } catch (SQLException ex) {
            Logger.getLogger(DaoUsuario.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
     public BeanUsuario buscarporcodigo(int codigo) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from usuario inner join tipousuario on usu_tipocod= tUsu_cod where usu_cod = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            usuario = criarUsuario(rs);
            return usuario;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Usuario não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
     public BeanUsuario salvar(BeanUsuario usu) throws SQLException {
        con = getConnection();
        try {
            if (usu.getUsu_cod()== 0) {
                String sql= "insert into usuario (usu_senha, usu_email,usu_tipocod,usu_endcod, usu_status) values (MD5(?),?,?,?,?)";
                pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                pstm = con.prepareStatement("update usuario set usu_senha = MD5(?), usu_email = ?, usu_tipocod = ?, usu_endcod = ?, usu_status = ? where usu_cod = ?");
            }
            System.out.println(usu.getUsu_senha());
            System.out.println(usu.getUsu_email());
            System.out.println(usu.getUsu_tusucod().gettUsu_cod());
            System.out.println(usu.getUsu_endcod().getEnd_cod());
            System.out.println(usu.getUsu_status());            
            
            pstm.setString(1, usu.getUsu_senha());
            pstm.setString(2, usu.getUsu_email());
            pstm.setInt(3, usu.getUsu_tusucod().gettUsu_cod());
            pstm.setInt(4, usu.getUsu_endcod().getEnd_cod());            
            pstm.setString(5, usu.getUsu_status());
            if (usu.getUsu_cod()> 0)//update
            {
                pstm.setInt(6, usu.getUsu_cod());
            }

            int count = pstm.executeUpdate();

            if (count == 0) {
                throw new SQLException("Erro ao salvar usuário");
            }

            if (usu.getUsu_cod()== 0) {
                usu.setUsu_cod(getGeneratedId(pstm));
            }

        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
        return usu;
    }

    public static int getGeneratedId(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            return id;
        }
        return 0;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Dao.BaseDao.getConnection;
import model.bean.BeanPaciente;

/**
 *
 * @author Perrone
 */
public class DaoPaciente {
    
    Connection con = model.Dao.BaseDao.getConnection();
    PreparedStatement pstm;
    ResultSet rs;
    BeanPaciente paciente = new BeanPaciente();
    List<BeanPaciente> list = new ArrayList<BeanPaciente>();

    public BeanPaciente buscarPorCpf(String cpf) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from paciente where pac_cpf = ?";
            pstm = con.prepareStatement(sql);
            pstm.setString(1, cpf);
            rs = pstm.executeQuery();
            paciente = criarPaciente(rs);
            return paciente;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Paciente não encontrado!" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }

    public BeanPaciente buscarPorCodigo(int codigo) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from paciente where pac_cod = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            paciente = criarPaciente(rs);
            return paciente;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Paciente não econtrado!" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }

    private BeanPaciente criarPaciente(ResultSet r) {
        list = new ArrayList<BeanPaciente>();
        try {
            while (rs.next()) {
                paciente = new BeanPaciente();
                paciente.setPac_cod(rs.getInt("pac_cod"));
                paciente.setPac_dtnasc(rs.getDate("pac_dtnasc"));
                paciente.setPac_tel(rs.getString("pac_tel"));
                paciente.setPac_sexo(rs.getString("pac_sexo"));                
                paciente.setPac_cpf(rs.getString("pac_cpf"));                             
                paciente.setPac_usucod(new DaoUsuario().buscarporcodigo(rs.getInt("clin_usucod")));
                list.add(paciente);
            }
            return paciente;
        } catch (SQLException ex) {
            Logger.getLogger(DaoClinica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void salvar(BeanPaciente pac, boolean up) throws SQLException {
        con = getConnection();
        try {
            if (!up) {
                System.out.println("INSERT PACIENTE");
                String sql = "insert into paciente(pac_dtnasc,pac_tel,pac_sexo, pac_cpf,pac_usucod) values (?,?,?,?,?)";
                pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                System.out.println("UPDATE PACIENTE: " + pac.getPac_cpf());
                pstm = con.prepareStatement("update paciente set pac_dtnasc = ?, pac_tel = ?, pac_sexo = ?, pac_cpf=?, pac_usucod = ? where pac_cpf = ?");
            }
            pstm.setDate(1, pac.getPac_dtnasc());
            pstm.setString(2, pac.getPac_tel());
            pstm.setString(3, pac.getPac_sexo());
            pstm.setString(4, pac.getPac_cpf());            
            pstm.setInt(5, pac.getPac_usucod().getUsu_cod());
            if (up)//update
            {
                pstm.setString(6, pac.getPac_cpf());
            }

            int count = pstm.executeUpdate();

            if (count == 0) {
                throw new SQLException("Erro ao salvar paciente! ");
            }

            if (pac.getPac_cpf() == null) {
                pac.setPac_cpf(pegarIdGerado(pstm));
            }

        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }

    public static String pegarIdGerado(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            String id = rs.getString(1);
            System.out.println("CPF: " + id);
            return id;
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Dao.BaseDao.getConnection;
import model.bean.BeanClinica;
import model.bean.BeanPagamento;

/**
 *
 * @author Perrone
 */
public class DaoPagamento {
    
    Connection con = model.Dao.BaseDao.getConnection();
    PreparedStatement pstm;
    ResultSet rs;
    BeanPagamento pag = new BeanPagamento();
    List<BeanPagamento> list = new ArrayList<BeanPagamento>();
    
    public List<BeanPagamento> listarTodos() throws SQLException {
        try {
            con = getConnection();
            String sql = "select * from pagamento";
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            pag = criarPagamento(rs);//preencher lista de Pagamento
            return list;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Pagamento não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public BeanPagamento buscarCodigo(int codigo) throws SQLException{      
         try{           
            con = getConnection();          
            String sql="select * from pagamento where pag_cod = ?";            
            pstm = con.prepareStatement(sql);           
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            pag= criarPagamento(rs);
            return pag;                     
        }catch(SQLException | HeadlessException erro){
            System.out.println("Pagamento Não Encontrado!" +erro.getMessage());            
            return null;
        }finally{
             con.close();
             rs.close();
             pstm.close();
         }       
    }
    
    public BeanPagamento buscarDesc(String desc) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from pagamento where pag_desc = ?";
            pstm = con.prepareStatement(sql);
            pstm.setString(1, desc);
            rs = pstm.executeQuery();
            pag = criarPagamento(rs);
            return pag;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Pagamento não encontrado!" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private BeanPagamento criarPagamento(ResultSet r) {
        list = new ArrayList<BeanPagamento>();
        try {
            while (rs.next()) {
                pag = new BeanPagamento();
                pag.setPag_cod(rs.getInt("pag_cod"));
                pag.setPag_desc(rs.getString("pag_desc"));                
                list.add(pag);
            }
            return pag;
        } catch (SQLException ex) {
            Logger.getLogger(DaoClinica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}

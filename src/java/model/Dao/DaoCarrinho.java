/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Dao.BaseDao.getConnection;
import model.bean.BeanCarrinho;

/**
 *
 * @author Perrone
 */
public class DaoCarrinho {
    
    Connection con = model.Dao.BaseDao.getConnection();
    PreparedStatement pstm;
    ResultSet rs;
    BeanCarrinho carrinho = new BeanCarrinho();
    List<BeanCarrinho> list = new ArrayList<BeanCarrinho>();

    public BeanCarrinho buscarPorCodigo(int codigo) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from carrinho where carrinho_cod = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            carrinho = criarCarrinho(rs);
            return carrinho;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Carrinho não encontrado!" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }

    private BeanCarrinho criarCarrinho(ResultSet r) {
        list = new ArrayList<BeanCarrinho>();
        try {
            while (rs.next()) {
                carrinho = new BeanCarrinho();
                carrinho.setCarrinho_cod(rs.getInt("carrinho_cod"));
                carrinho.setCarrinho_status(rs.getString("carrinho_status"));
                carrinho.setCarrinho_paccod(new DaoPaciente().buscarPorCodigo(rs.getInt("carrinho_paccod")));
                carrinho.setCarrinho_pagcod(new DaoPagamento().buscarCodigo(rs.getInt("carrinho_pagcod")));                
                list.add(carrinho);
            }
            return carrinho;
        } catch (SQLException ex) {
            Logger.getLogger(DaoClinica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void salvar(BeanCarrinho carrinho, boolean up) throws SQLException {
        con = getConnection();
        try {
            if (!up) {
                System.out.println("INSERT CARRINHO");
                String sql = "insert into carrinho(carrinho_status,carrinho_paccod,carrinho_pagcod) values (?,?,?)";
                pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                System.out.println("UPDATE CARRINHO:" + carrinho.getCarrinho_cod());
                pstm = con.prepareStatement("update carrinho set carrinho_status = ?, carrinho_paccod = ?, carrinho_pagcod = ? where carrinho_cod = ?");
            }

            pstm.setString(1, carrinho.getCarrinho_status());            
            pstm.setInt(2, carrinho.getCarrinho_paccod().getPac_cod());
            pstm.setInt(3, carrinho.getCarrinho_pagcod().getPag_cod());
            if (up)//update
            {
                pstm.setInt(4, carrinho.getCarrinho_cod());
            }

            int count = pstm.executeUpdate();

            if (count == 0) {
                throw new SQLException("Erro ao salvar carrinho! ");
            }

            if (carrinho.getCarrinho_cod() == 0) {
                carrinho.setCarrinho_cod(pegarIdGerado(pstm));
            }

        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }

    public static int pegarIdGerado(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            System.out.println("Codigo: " + id);
            return id;
        }
        return 0;
    }
}

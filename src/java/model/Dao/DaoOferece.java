/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Dao.BaseDao.getConnection;
import model.bean.BeanOferece;

/**
 *
 * @author Perrone
 */
public class DaoOferece {
    
    
Connection con = model.Dao.BaseDao.getConnection();
    PreparedStatement pstm;
    ResultSet rs;
    BeanOferece oferece = new BeanOferece();
    List<BeanOferece> list = new ArrayList<BeanOferece>();
    
    public BeanOferece buscarCodigo(int codigo) throws SQLException{      
         try{           
            con = getConnection();          
            String sql="select * from oferece where ofe_cod = ?";            
            pstm = con.prepareStatement(sql);           
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            oferece= criarOferece(rs);
            return oferece;                     
        }catch(SQLException | HeadlessException erro){
            System.out.println("Oferece não encontrado!" +erro.getMessage());            
            return null;
        }finally{
             con.close();
             rs.close();
             pstm.close();
         }       
    }
    
     public BeanOferece salvarOferece(BeanOferece ofe) throws SQLException {
        con = BaseDao.getConnection();
        int codOfe =0;
        try {
            if (ofe.getOfe_cod()== 0) {
                System.out.println("INSERT Oferece");
                    String sql = "insert into oferece(ofe_valor, ofe_servcod,ofe_clincod,ofe_status) values (?,?,?,?)";
                pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                System.out.println("UPDATE Oferece");
                pstm = con.prepareStatement("update oferece set ofe_valor=?, ofe_servcod=?,ofe_clincod=?, ofe_status=? where ofe_cod= ?");
            }
            pstm.setDouble(1,ofe.getOfe_valor());
            pstm.setInt(2, ofe.getOfe_servcod().getServ_cod());
            pstm.setInt(3, ofe.getOfe_clincod().getClin_cod());
            pstm.setString(4,ofe.getOfe_status());
            
            
            if (ofe.getOfe_cod()> 0)//update
            {
                pstm.setInt(5, ofe.getOfe_cod());
            }

            int count = pstm.executeUpdate();

            if (count == 0) {
                throw new SQLException("Erro ao salvar Oferece!");
            }

            if (ofe.getOfe_cod()== 0) {
                ofe.setOfe_cod(pegarCodigoGerado(pstm));
            }
        
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
        return oferece;
    }
   
   
    private BeanOferece criarOferece(ResultSet r) {        
        try {
            while (rs.next()) {
                oferece = new BeanOferece();
                oferece.setOfe_cod(rs.getInt("ofe_cod"));
                oferece.setOfe_valor(rs.getDouble("ofe_valor"));                              
                oferece.setOfe_status(rs.getString("ofe_status"));
                oferece.setOfe_servcod(new DaoServico().buscarCodigo(rs.getInt("ofe_servcod")));
                oferece.setOfe_clincod(new DaoClinica().buscarPorCodigo(rs.getInt("ofe_clincod")));
            }
            return oferece;
        } catch (SQLException ex) {
            Logger.getLogger(DaoClinica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static int pegarCodigoGerado(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            return id;
        }
        return 0;
    }
}    
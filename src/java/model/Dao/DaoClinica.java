/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Dao.BaseDao.getConnection;
import model.bean.BeanClinica;

/**
 *
 * @author S1PPERRONE
 */
public class DaoClinica {

    Connection con = model.Dao.BaseDao.getConnection();
    PreparedStatement pstm;
    ResultSet rs;
    BeanClinica clinica = new BeanClinica();
    List<BeanClinica> list = new ArrayList<BeanClinica>();

    public BeanClinica buscarPorCnpj(String cnpj) throws SQLException {
        con = getConnection();
        try {
            String sql = "se lect * from clinica where clin_cnpj = ?";
            pstm = con.prepareStatement(sql);
            pstm.setString(1, cnpj);
            rs = pstm.executeQuery();
            clinica = criarClinica(rs);
            return clinica;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Clinica não encontrada!" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }

    public BeanClinica buscarPorCodigo(int codigo) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from clinica where clin_cod = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            clinica = criarClinica(rs);
            return clinica;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Clinica não encontrada!" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }

    private BeanClinica criarClinica(ResultSet r) {
        list = new ArrayList<BeanClinica>();
        try {
            while (rs.next()) {
                clinica = new BeanClinica();
                clinica.setClin_cod(rs.getInt("clin_cod"));
                clinica.setClin_cnpj(rs.getString("clin_cnpj"));
                clinica.setClin_razao(rs.getString("clin_razao"));
                clinica.setClin_segcod(new DaoSegmento().buscarPorCodigo(rs.getInt("clin_segcod")));
                clinica.setClin_usucod(new DaoUsuario().buscarporcodigo(rs.getInt("clin_usucod")));
                list.add(clinica);
            }
            return clinica;
        } catch (SQLException ex) {
            Logger.getLogger(DaoClinica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void salvar(BeanClinica clin, boolean up) throws SQLException {
        con = getConnection();
        try {
            if (!up) {
                System.out.println("INSERT CLINICA");
                String sql = "insert into clinica(clin_cnpj,clin_razao,clin_segcod, clin_usucod) values (?,?,?,?)";
                pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                System.out.println("UPDATE CLINICA:" + clin.getClin_cnpj());
                pstm = con.prepareStatement("update clinica set clin_cnpj = ?, clin_razao = ?, clin_segcod = ?, clin_usucod = ? where clin_cnpj = ?");
            }

            pstm.setString(1, clin.getClin_cnpj());
            pstm.setString(2, clin.getClin_razao());
            pstm.setInt(3, clin.getClin_segcod().getSeg_cod());
            pstm.setInt(4, clin.getClin_usucod().getUsu_cod());
            if (up)//update
            {
                pstm.setString(5, clin.getClin_cnpj());
            }

            int count = pstm.executeUpdate();

            if (count == 0) {
                throw new SQLException("Erro ao salvar clínica! ");
            }

            if (clin.getClin_cnpj() == null) {
                clin.setClin_cnpj(pegarIdGerado(pstm));
            }

        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }

    public static String pegarIdGerado(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            String id = rs.getString(1);
            System.out.println("CNPJ: " + id);
            return id;
        }
        return null;
    }
}

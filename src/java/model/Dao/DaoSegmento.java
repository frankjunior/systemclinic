/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Connection.Connection.getConnection;
import model.bean.BeanSegmento;

/**
 *
 * @author S1PPERRONE
 */
public class DaoSegmento {
    Connection con;
    PreparedStatement pstm;
    ResultSet rs;
    BeanSegmento seg = new BeanSegmento();
    List<BeanSegmento> list;
    
    public List<BeanSegmento> listarTodos() throws SQLException {
        try {
            con = getConnection();
            String sql = "select * from segmento";
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            seg = criarSegmento(rs);//preencher lista de segmento
            return list;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Segmento não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public BeanSegmento buscarporDescricao(String desc) throws SQLException {
        try {
            con = getConnection();
            System.out.println("Buscar cod seg por desc");
            String sql = "select * from segmento where seg_desc = ?";
            pstm = con.prepareStatement(sql);
            pstm.setString(1, desc);
            rs = pstm.executeQuery();
            seg = criarSegmento(rs);
            return seg;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Segmento não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public BeanSegmento buscarPorCodigo(int codigo) throws SQLException {
        con = getConnection();
        try {
            String sql = "select * from segmento where seg_cod = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            seg = criarSegmento(rs);
            return seg;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Segmento não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
     
     private BeanSegmento criarSegmento(ResultSet r){
        try {
            while (rs.next()) {
                seg.setSeg_cod(r.getInt("seg_cod"));
                seg.setSeg_desc(r.getString("seg_desc"));                 
            }
            return seg;
        } catch (SQLException ex) {
            Logger.getLogger(DaoBairro.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
     }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.Dao.BaseDao.getConnection;
import model.bean.BeanBairro;

/**
 *
 * @author S1PPERRONE
 */
public class DaoBairro {
    
    Connection con;
    PreparedStatement pstm;
    ResultSet rs;
    BeanBairro bairro = new BeanBairro();
    List<BeanBairro> list;
    
    public List<BeanBairro> listarTodos() throws SQLException {
        try {
            con = getConnection();
            String sql = "select * from bairro";
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            bairro = criarBairro(rs);//preencher lista de bairro
            return list;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Bairro não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public BeanBairro buscarporDescricao(String desc) throws SQLException {
        try {
            con = getConnection();
            String sql = "select * from bairro where bai_desc = ?";
            pstm = con.prepareStatement(sql);
            pstm.setString(1, desc);
            rs = pstm.executeQuery();
            bairro = criarBairro(rs);
            return bairro;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Bairro não encontrado" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public BeanBairro buscarCodigo(int codigo) throws SQLException{      
         try{           
            con = getConnection();          
            String sql="select * from bairro where bai_cod = ?";            
            pstm = con.prepareStatement(sql);           
            pstm.setInt(1, codigo);
            rs = pstm.executeQuery();
            bairro= criarBairro(rs);
            return bairro;                     
        }catch(SQLException | HeadlessException erro){
            System.out.println("Bairro Não Encontrado!" +erro.getMessage());            
            return null;
        }finally{
             con.close();
             rs.close();
             pstm.close();
         }       
    }
     
     private BeanBairro criarBairro(ResultSet r){
        try {
            while (rs.next()) {
                bairro.setBai_cod(r.getInt("bai_cod"));
                bairro.setBai_desc(r.getString("bai_desc"));                 
            }
            return bairro;
        } catch (SQLException ex) {
            Logger.getLogger(DaoBairro.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
     }
}

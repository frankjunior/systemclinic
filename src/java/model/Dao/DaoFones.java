/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.bean.BeanFones;

/**
 *
 * @author S1PPERRONE
 */
public class DaoFones {
    
    Connection con;
    PreparedStatement pstm;
    ResultSet rs;
    BeanFones fones = new BeanFones();
    List<BeanFones> list;
    
    public BeanFones salvarFone(BeanFones fon) throws SQLException {
        con = BaseDao.getConnection();
        int codFon =0;
        try {
            if (fon.getFon_cod()== 0) {
                System.out.println("INSERT Fone");
                String sql = "insert into fonesclin(fon_num, fon_num2, fon_clincod) values (?,?,?)";
                pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                System.out.println("UPDATE Fone");
                pstm = con.prepareStatement("update fonesclin set fon_num=?, fon_num2=? where fon_cod = ?");
            }
            
            pstm.setString(1, fon.getFon_num1());
            pstm.setString(2, fon.getFon_num2());
            pstm.setInt(3, fon.getFon_clincod().getClin_cod());
            
            if (fon.getFon_cod()> 0)//update
            {
                pstm.setInt(3, fon.getFon_cod());
            }

            int count = pstm.executeUpdate();

            if (count == 0) {
                throw new SQLException("Erro ao salvar Fones da clinica");
            }

            if (fon.getFon_cod()== 0) {
                fon.setFon_cod(getGeneratedId(pstm));
            }
        
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
        return fones;
    }
    
    public BeanFones buscarPorClinica(int cliCod) throws SQLException {
        con = BaseDao.getConnection();
        try {
            String sql = "select * from fonesclin where fon_clincod = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, cliCod);
            rs = pstm.executeQuery();
            fones = criarFone(rs);
            return fones;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Fone não encontrado!" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private BeanFones criarFone(ResultSet r) {
        list = new ArrayList<BeanFones>();
        try {
            while (rs.next()) {
                fones = new BeanFones();
                fones.setFon_clincod(new DaoClinica().buscarPorCodigo(rs.getInt("fon_clincod")));
                fones.setFon_cod(rs.getInt("fon_cod"));
                fones.setFon_num1(rs.getString("fon_num"));
                fones.setFon_num2(rs.getString("fon_num2"));              
                list.add(fones);
            }
            return fones;
        } catch (SQLException ex) {
            Logger.getLogger(DaoClinica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static int getGeneratedId(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            return id;
        }
        return 0;
    }
}



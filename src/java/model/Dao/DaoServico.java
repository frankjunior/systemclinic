/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Dao;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.bean.BeanServico;

/**
 *
 * @author n0mercy
 */
public class DaoServico {
    Connection con;
    PreparedStatement pstm;
    ResultSet rs;
    BeanServico servico;
    List<BeanServico> list;
    
    public BeanServico salvarServico(BeanServico s) throws SQLException {
        con = BaseDao.getConnection();
        int codServ =0;
        try {
            if (s.getServ_cod() == 0) {
                String sql = "insert into servico(fon_num) values (?)";
                pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                pstm = con.prepareStatement("update servico set serv_desc = ? where serv_cod = ?");
            }
            
            pstm.setString(1, s.getServ_desc());       
            
            if (s.getServ_cod() > 0)//update
            {
                pstm.setInt(2, s.getServ_cod());
            }

            int count = pstm.executeUpdate();

            if (count == 0) {
                throw new SQLException("Erro ao salvar Serviço");
            }

            if (s.getServ_cod() == 0) {
                s.setServ_cod(getGeneratedId(pstm));
            }
        
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
        return s;
    }
    
    public BeanServico buscarCodigo(int cliCod) throws SQLException {
        con = BaseDao.getConnection();
        try {
            String sql = "select * from servico where serv_cod = ?";
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, cliCod);
            rs = pstm.executeQuery();
            servico = criarServico(rs);
            return servico;
        } catch (SQLException | HeadlessException erro) {
            System.out.println("Fone não encontrado!" + erro.getMessage());
            return null;
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            if (con != null) {
                con.close();
            }

            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private BeanServico criarServico(ResultSet r) {      
        servico = new BeanServico();
        try {
            while (rs.next()) {
                servico = new BeanServico();                
                servico.setServ_cod(rs.getInt("serv_cod"));
                servico.setServ_desc(rs.getString("fon_num2"));
            }
            return servico;
        } catch (SQLException ex) {
            Logger.getLogger(DaoClinica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static int getGeneratedId(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            return id;
        }
        return 0;
    }
}

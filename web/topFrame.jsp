<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Principal</title>
        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
                  Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
            <script type="application/x-javascript">
                addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
                }, false);

                function hideURLbar() {
                window.scrollTo(0, 1);
                }
            </script>
            <!--//tags -->
            <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
            <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
            <link href="css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
            <link href="css/font-awesome.css" rel="stylesheet">
                <!-- //for bootstrap working -->
                <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
                    <style type="text/css">
                        <!--
                        body,td,th {
                            font-size:70%;
                        }
                        -->
                    </style></head>
                    <body>
                        <div class="top_menu_w3layouts">
                            <div class="container">			
                                <div class="header_right">
                                    <ul class="forms_right">
                                        <li><a href="ControllerLogin" onclick="window.close()" target="login.jsp"> Logout</a> </li>
                                    </ul>
                                </div>

                                <span style="color: white"><%
                                    if (session.getAttribute("user") != null) {
                                        out.print(session.getAttribute("user").toString());
                                    }
                                    %>
                                </span>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <!-- header -->
                        <div class="header" id="home">	
                            <div class="content white">
                                <nav class="navbar navbar-default" role="navigation">
                                    <div class="container">					
                                        <!--/.navbar-header-->
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                            <nav>
                                                <ul class="nav navbar-nav">
                                                     <% if (session.getAttribute("role").toString().equalsIgnoreCase("3")) { %>
                                                    <li><a href="pages/buscarServico.jsp" target="mainFrame" >Buscar Servi�os</a></li>
                                                     <%}%>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Clinica<b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("2")) { %>
                                                            <li><a href="cadastro/edtClinica.jsp" target="mainFrame">Alterar</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>
                                                                <% if (session.getAttribute("role").toString().equalsIgnoreCase("1")) { %>
                                                            <li><a href="pages/buscarClinica.jsp" target="mainFrame">Buscar</a></li>
                                                            <li class="divider"></li>
                                                                <%}%>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Paciente<b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                             <% if (session.getAttribute("role").toString().equalsIgnoreCase("3")) { %>
                                                            <li><a href="cadastro/cadPaciente.jsp" target="mainFrame">Alterar</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>
                                                             <% if (session.getAttribute("role").toString().equalsIgnoreCase("1")) { %>
                                                            <li><a href="buscarPaciente.html" target="mainFrame">Buscar</a></li>
                                                            <li class="divider"></li>
                                                            <%}%>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Relat�rio<b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("2")) { %>
                                                            <li><a href="#">Bilhetes Vendidos</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("3")) { %>
                                                            <li><a href="#">Bilhetes Comprados</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("1")) { %>
                                                            <li><a href="#">Empresas Ativas</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>                                                                                
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cadastro<b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                             <% if (session.getAttribute("role").toString().equalsIgnoreCase("2")) { %>
                                                            <li><a href="pages/funcionario.jsp" target="mainFrame">Funcionario</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("2")) { %>
                                                            <li><a href="pages/servicoVenda.jsp" target="mainFrame">Servi�os Venda</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>                                                            
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("1")) { %>
                                                            <li><a href="pages/servico.jsp" target="mainFrame">Servi�os</a></li>
                                                            <li class="divider"></li>
                                                            <%}%>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultar<b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("2")) { %>
                                                            <li><a href="#">Consultar Agenda</a></li>
                                                             <%}%>
                                                            <li class="divider"></li>
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("2")) { %>
                                                            <li><a href="pages/consultaBilhete.jsp" target="mainFrame">Consultar Bilhete</a></li>
                                                             <%}%>
                                                            <li class="divider"></li>
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("2")) { %>
                                                            <li><a href="#">Consultar Mensalidade</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>	
                                                            <% if (session.getAttribute("role").toString().equalsIgnoreCase("2")) { %>
                                                            <li><a href="pages/servicoVenda.jsp">Consultar Servi�os Oferecidos</a></li>
                                                            <%}%>
                                                            <li class="divider"></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                        <!--/.navbar-collapse-->
                                        <!--/.navbar-->
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="banner_inner_content_agile_w3l">

                        </div>
                        <div align="center">
                            <!-- js -->
                            <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
                            <script>
                                            $('ul.dropdown-menu li').hover(function () {
                                                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);

                                            }, function () {
                                                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
                                            });
                            </script>
                            <script type="text/javascript" src="js/bootstrap.js"></script>
                        </div>
                        </html>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<head>
	<title>Consultar Bilhete </title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
</head>

<body>
    <div class="w3ls-banner">
		<div class="container_1">
			<div class="heading">
				<h2>Consultar Bilhete</h2>
				<p>Digite o c�digoo do bilhete para consultar!</p>
			</div>
			<div class="agile-form">
				<form action="#" method="post">
					<ul class="field-list">						                                                                                                
						  <li>
							<label class="form-label"> 
								C�digos do Bilhete
								<span class="form-required"> * </span>
							</label>
							<div class="input-group">
							    <input type="text" name="codPesBilhete" placeholder="Digite o código do bilhete..." required >                                                                
                                                                <span class="input-group-btn">
                                                                        <button class="btn btn-default" type="button">Buscar Bilhete</button>
                                                                </span>
                                                        </div>
						</li>
                                                <li> 
							<label class="form-label">
							  C�digos							   
							</label>
							<div class="form-input">
								<input type="text" name="codigo" placeholder=" " required >
							</div>
						</li>					                                                                                                                                                                                                                                                                                                                                                                                                
                                                <li> 
							<label class="form-label">
							  Nome							   
							</label>
							<div class="form-input">
								<input type="text" name="nome" placeholder="Nome Paciente" required >
							</div>
						</li>
                                                <li> 
							<label class="form-label">
							   Cpf							   
							</label>
							<div class="form-input">
								<input type="text" name="cpf" placeholder="Cpf" required >
							</div>
						</li>
                                                <li> 
							<label class="form-label">
							   E-mail							   
							</label>
							<div class="form-input">
								<input type="text" name="email" placeholder="E-mail" required >
							</div>
						</li>  
                                                <li> 
							<label class="form-label">
							   Data do Agendamento							   
							</label>
							<div class="form-input">
								<input type="text" name="dtAgendamento" placeholder="Data do agendamento" required >
							</div>
						</li> 
                                                <li> 
							<label class="form-label">
							   Data do Atendimento						   
							</label>
							<div class="form-input">
								<input type="text" name="dtAtendimento" placeholder="Data do Atendimento" required >
							</div>
						</li>
                                                <li> 
							<label class="form-label">
							   Funcion�rio que realizou atendimento							   
							</label>
							<div class="form-input">
								<input type="text" name="funcAtendimento" placeholder="Nome do funcion�rios" required >
							</div>
						</li>
                                                <li> 
							<label class="form-label">
							   Status do Bilhete							   
							</label>
							<div class="form-input">
								<input type="text" name="status" placeholder="Status do bilhete" required >
							</div>
						</li>
                                                <li> 
							<label class="form-label">
							   Data da Compra							   
							</label>
							<div class="form-input">
								<input type="text" name="dtCompra" placeholder="Data da compra" required >
							</div>
						</li>
                                                <li>
                                                    <div class="bs-docs-example">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Cod Servi�o</th>
							<th>Valor Item</th>
                                                        <th>Quantidade</th>                                                        
						</tr>
					</thead>
					<tbody>                                       
						<tr>
							<td>1</td>
							<td></td>
							<td></td>                                                        
						</tr>                                                                                     
						<tr>
							<td>2</td>
							<td></td>
							<td></td>                                                      
						</tr>
						<tr>
							<td>3</td>
							<td></td>
							<td></td>                                                        
						</tr>
					</tbody>
				</table>
			</div>                                                    
                                </li>
                                <li> 
                                	<label class="form-label">
                       			   Valor Total Bilhete						   
                                        </label>
                                        <div class="form-input">
			    		<input type="text" name="vlrTotal" placeholder="Valor Total" required >
					</div>
				</li>                                     
                                    </ul>
                                </form>                                                                                                                  
                         </div>
               </div>
    </div>
    
                          
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>


</html>
<%@page import="model.Dao.DaoClinica"%>
<%@page import="model.bean.BeanClinica"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

    <head>
        <title>Buscar Cl�nica</title>
        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript">
            addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
            window.scrollTo(0, 1);
            }
        </script>
        <!--//tags -->
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/font-awesome.css" rel="stylesheet">
        <!-- //for bootstrap working -->
        <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
    </head>

    <body>
        <!-- header -->

        <%
            BeanClinica clinica = new BeanClinica();
            if (request.getParameter("search_clinica") != null) {
                clinica = new DaoClinica().buscarPorCnpj(request.getParameter("search_clinica"));
            } else {
                System.out.println("Clinica n�o encontrada.");
            }
        %>
        <p></p>
        <p></p>
        <div class="w3ls-banner">
            <div class="container_1">
                <div class="heading">
                    <h2>Ativar/Desativar Cl�nica</h2>	
                    <p></p>
                </div>

                <div class="agile-form">

                    <ul class="field-list">
                        <li>
                            <form method="get" action="#">
                                <label class="form-label"> 
                                    CNPJ
                                    <span class="form-required"> * </span>
                                </label>
                                <div class="input-group">
                                    <input type="text" name="search_clinica" placeholder="Digite o cnpj da cl�nica que deseja..." required >                                                                
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Buscar Cl�nica</button>
                                    </span>
                                </div>
                            </form>
                        </li>
                        <form method="put" action="../ControllerClinica">
                            <li>
                                <label class="form-label"> 
                                    Cnpj 								
                                </label>
                                <div class="form-input">
                                    <input type="text" readonly="true" value="<%= clinica.getClin_cnpj() != null ? clinica.getClin_cnpj() : ""%>" name="cnpj" placeholder="cnpj" required >
                                </div>
                            </li>
                            <li>
                                <label class="form-label"> 
                                    Raz�o Social 								
                                </label>
                                <div class="form-input">
                                    <input type="text" readonly="true" value="<%= clinica.getClin_razao() != null ? clinica.getClin_razao() : ""%>" name="razao" placeholder="Razão Social" required >
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    E-Mail							   
                                </label>
                                <div class="form-input">
                                    <input type="email" readonly="true" value="<%= clinica.getClin_usucod() != null ? clinica.getClin_usucod().getUsu_email() : ""%>" name="email" placeholder="e-mail" required>
                                </div>
                            </li>
                            <li>
                                <label class="form-label">
                                    Segmento							   
                                </label>
                                <div class="form-input">
                                    <select class="form-dropdown" name="segmento">
                                        <option value=""><%= clinica.getClin_segcod() != null ? clinica.getClin_segcod().getSeg_desc() : ""%></option>
                                    </select>
                                </div>
                            </li>  
                            <li>
                                <label class="form-label">
                                    Status							   
                                </label>
                                <div class="form-input">
                                    <select class="form-dropdown" name="status">
                                        <%if (clinica.getClin_usucod() != null && clinica.getClin_usucod().getUsu_status().equals("ativo")) {%>
                                        <option value="ativo" selected="true">Ativo</option>
                                        <option value="inativo"> Desativado </option>                                                                        
                                        <%} else {%>
                                        <option value="ativo">Ativo</option>
                                        <option value="inativo" selected="true"> Desativado </option> 
                                        <%}%>
                                    </select>
                                </div>
                            </li>
                            <% if (clinica.getClin_cnpj() != null) {%>
                            <li>
                                <label class="form-label">A��o</label>
                                <div class="form-input">
                                    <input type="hidden" value="buscarClinica" name="page"/>
                                    <input type="hidden" value="<%= clinica.getClin_usucod().getUsu_cod()%>" name="usuario_id"/>
                                    <button class="btn-primary" type="submit"><%= (clinica.getClin_usucod() != null && clinica.getClin_usucod().getUsu_status().equalsIgnoreCase("ativo")) ? "Desativar" : "Ativar"%></button>
                                </div>
                            </li>
                            <% }%>
                        </form>
                    </ul>					

                </div>
            </div>
        </div>
        <!-- js -->
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script>
            $('ul.dropdown-menu li').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            });
        </script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
    </body>

</html>
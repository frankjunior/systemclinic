<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<head>
	<title>Servi�os </title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
</head>

<body>
    <div class="w3ls-banner">
		<div class="container_1">
			<div class="heading">
				<h2>Servi�os</h2>
				<p>Para cadastrar/alterar, voc� deve primeiro buscar!</p>
			</div>
			<div class="agile-form">
				<form action="#" method="post">
					<ul class="field-list">						                                                                                                
						<li> 
							<label class="form-label">
							  C�digo							   
							</label>
							<div class="form-input">
								<input type="text" name="codigo" placeholder=" " required >
							</div>
						</li>
						<li> 
							<label class="form-label">
							   Descri��o
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<input type="text" name="descricao" placeholder="Descri��o do servi�o" required >
							</div>
						</li>
                                                <li>
							<label class="form-label">
							   Segmento
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<select class="form-dropdown" name="sexo" required>
									<option value="">&nbsp;</option>
									<option value="ativo"> Odontologico </option>
									<option value="inativo"> Estetico </option>
                                                                        <option value="inativo"> Plastico </option>
								</select>
							</div>
                                                </li>
                                                <li>
							<label class="form-label">
							   Status
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<select class="form-dropdown" name="status" required>
									<option value="">&nbsp;</option>
									<option value="ativo"> Ativo </option>
									<option value="inativo"> Inativo </option>									
								</select>
							</div>
                                                </li>
                                                <li>
							<label class="form-label"> 
								Descri��o do Servi�o
								<span class="form-required"> * </span>
							</label>
							<div class="input-group">
							    <input type="text" name="descPesServ" placeholder="Digite o nome do serviço que deseja..." required >                                                                
                                                                <span class="input-group-btn">
                                                                        <button class="btn btn-default" type="button">Buscar Servi�o</button>
                                                                </span>
                                                        </div>
						</li>
                                                <li>
                                                    <div class="bs-docs-example">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>C�digo do Servi�o</th>
							<th>Descri��os</th>
                                                        <th>Segmento</th>
							<th>Status</th>							
						</tr>
					</thead>
					<tbody>                                       
						<tr>
							<td>1</td>
							<td></td>
                                                        <td></td>
							<td></td>							
						</tr>                                                                                     
						<tr>
							<td>2</td>
							<td></td>
							<td></td>
                                                        <td></td>
						</tr>
						<tr>
							<td>3</td>
							<td></td>
                                                        <td></td>
							<td></td>							
						</tr>
					</tbody>
				</table>
			</div>
                                                    
                                                </li>
                                    <li>
                                        <div class="input-group">
                                            <span>
                                                <button class="btn-primary" type="submit">Novo Cadastro</button>
                                                <button class="btn-primary" type="submit">Alterar Cadastro</button>
                                                <button class="btn-primary" type="submit">Salvar Dados</button>
                                                <button class="btn-primary" type="submit">Cancelar</button>
                                            </span> 
                                        </div>
                                    </li>  
                                    </ul>
                                </form>                                                                                                                  
                         </div>
               </div>
    </div>
    
                          
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>

</html>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<head>
	<title>Carrinho</title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
</head>

<body>
	<!-- header -->
	<div class="header" id="home">		

		<div class="content white">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="navbar-header">				
						<a class="navbar-brand" href="#">
							<h1><span class="fa fa-stethoscope" aria-hidden="true"></span>Nome XXXXX </h1>
						</a>
					</div>
					<!--/.dados empresa-->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<nav>
							<ul class="nav navbar-nav">
                                                                <li><a style="font-style: italic ">Cnpj: xx.xxx.xxx.xxxx-xx</a></li>
								<li><a style="font-style: italic ">Telefone: (xx)xxxxx-xxxx</a></li>								
								<li><a style="font-style: italic ">Cep: xxxxxxx-xx</a></li>								
                                                                <li><a style="font-style: italic ">Rua: xxxxxxxxxxxxxxx</a></li>								
                                                        </ul>
                                                </nav>
                                        </div>
                                </div>
                        </nav>
                </div>
        </div>			
                			<!--/.Tabela de serviços adicionados-->
			<div class="bs-docs-example">
                         <label class="form-label"> 
				Servi�os Adicionados
                            </label>
                            <form action="#">
                            <table class="table table-striped">
					<thead>
						<tr>
							<th>C�digo Servi�o</th>
							<th>Descri��o</th>
							<th>Valor Unit�rio</th>
							<th>Remover Item</th>                                                        
						</tr>
					</thead>
					<tbody>                                       
						<tr>
							<td>1</td>
							<td></td>
							<td></td>						
                                                        <td><button type="submit" >Remover</button></td>
						</tr>                                                                                     
						<tr>
							<td>2</td>
							<td></td>
							<td></td>
							<td><button type="submit" >Remover</button></td>
						</tr>
						<tr>
							<td>3</td>
							<td></td>
							<td></td>
							<td><button type="submit" >Remover</button></td>                                                        
						</tr>
					</tbody>
				</table>
			</form>
                            <a></a>
                         <label class="form-label"> 
			     Buscar novo servi�o
                            </label>
                        <div style="align-content: center" class="col-md-9" >													
				<div class="col-lg-6 in-gp-tb">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Descri��o do novo servi�o que deseja buscar...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">Buscar!</button>
						</span>
					</div><!-- /input-group -->
				</div><!-- /.col-lg-6 -->
			</div><!-- /.row -->		
                        </div>
                        <label class="form-label"> 
			     Servi�o � adicionar
                            </label>
                         <form action="#">
                            <table class="table table-striped">
					<thead>
						<tr>
							<th>C�digo Servi�o</th>
							<th>Descri��o</th>
							<th>Valor Unit�rio</th>
							<th>Adicionar Item</th>                                                        
						</tr>
					</thead>
					<tbody>                                       
						<tr>
							<td>1</td>
							<td></td>
							<td></td>						
                                                        <td><button type="submit" >Adicionar</button></td>
						</tr>                                                                                     
						<tr>
							<td>2</td>
							<td></td>
							<td></td>
							<td><button type="submit" >Adicionar</button></td>
						</tr>
						<tr>
							<td>3</td>
							<td></td>
							<td></td>
							<td><button type="submit" >Adicionar</button></td>                                                        
						</tr>
					</tbody>
				</table>
			</form>
                                        <form action="caixa.jsp" target="mainFrame">
                                        <div class="header" id="home">		
		<div class="content white">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">					
					<!--/.dados empresa-->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<nav>
							<ul class="nav navbar-nav">
                                                                <button class="btn-primary" type="submit">Finalizar Bilhete</button>
                                                                <button class="btn-primary" type="submit">Cancelar Bilhete</button>       								
                                                                <li><a style="font-style: italic ">Valor Total: xxxxxxxxxxxxxxx</a></li>								
                                                        </ul>
                                                </nav>
                                        </div>
                                </div>
                        </nav>
                </div>
        </div>
                                        </form>                               
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>

</html>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<head>
	<title>Caixa</title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
</head>

<body>
    <div class="w3ls-banner">   
		<div class="container_1">
			<div class="heading">
				<h2>Caixa</h2>
				<p>Confirme as informa��es abaixo para finalizar pagamento do Bilhete.</p>                                
			</div>
                            <label class="form-label"> 
				Servi�os Adicionados		
			</label>
                        <form action="#">
                            <table class="table table-striped">
					<thead>
						<tr>
							<th>C�digo Servi�o</th>
							<th>Descri��o</th>
							<th>Valor Unit�rio</th>							
						</tr>
					</thead>
					<tbody>                                       
						<tr>
							<td>1</td>
							<td></td>
							<td></td>						                                                       
						</tr>                                                                                     
						<tr>
							<td>2</td>
							<td></td>
							<td></td>							
						</tr>
						<tr>
							<td>3</td>
							<td></td>
							<td></td>							
						</tr>
					</tbody>
				</table>
			</form>
			<div class="agile-form">
				<form action="/" method="post">
					<ul class="field-list">
						<li>
							<label class="form-label"> 
								Valor Total:								
							</label>
							<div class="form-input">
								<input type="text" name="vlr_total" placeholder="85,00 R$" required >
							</div>
						</li>
						<li>
							<label class="form-label">
							   Forma de Pagamento
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<select class="form-dropdown" name="tipo_pagamento" required>									
									<option value="Male"> D�bito </option>
									<option value="Female"> Cr�dito </option>									
								</select>
							</div>
						<li>
						
					</ul>
					<input type="submit" value="FINALIZAR PAGAMENTO">
                                        <input type="submit" value="VOLTAR PARA CARRINHO">
				</form>	
			</div>
		</div>
    </div>
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>

</html>
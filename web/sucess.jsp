<%-- 
    Document   : login
    Created on : 22/03/2018, 14:14:58
    Author     : Kaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>

        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/animatelogin.css">
        <!-- Custom Stylesheet -->
        <link rel="stylesheet" href="css/stylelogin.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    </head>
    <body>
        <form action="" method="post">
            <div class="container">		
                <div class="login-box animated fadeInUp">
                    <div class="box-header">
                        <h2><%= request.getParameter("title") %></h2>
                    </div>
                        <%= request.getParameter("msg") %>
                        <br/>
                        <br/>
                        <button type="button" onclick="document.location.href='<%= request.getParameter("redirect")%>'">Continuar</button>
                    <br/>
                </div>
            </div>
        </form>
    </body>
</html>

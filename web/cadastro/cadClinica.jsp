<%@page import="java.util.ArrayList"%>
<%@page import="model.Dao.BaseDao"%>
<%@page import="java.util.List"%>
<%@page import="model.bean.BeanBairro"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.Connection"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>

        <title>Cad - Clinica </title>
        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript">
            addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
            window.scrollTo(0, 1);
            }
        </script>
        <!--//tags -->
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/font-awesome.css" rel="stylesheet">
        <!-- //for bootstrap working -->
        <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
    </head>
    <body>	 
        <%
            int tipo_usu = 2;
            Connection con;
            PreparedStatement pstm;
            ResultSet rs;
            BeanBairro bairro = new BeanBairro();
            con = BaseDao.getConnection();
        %>
        <p></p>
        <p></p>
        <div class="w3ls-banner">
            <div class="container_1">
                <div class="heading">
                    <h2>Cl�nica</h2>	
                    <p></p>
                </div>
                <div class="agile-form">
                    <form action="../ControllerClinica" method="post">
                        <ul class="field-list">
                            <li>
                                <label class="form-label"> 
                                    Cnpj 

                                </label>
                                <div class="form-input">
                                    <input type="text" name="cnpj" placeholder="Entre com o cnpj" required >
                                </div>
                            </li>
                            <li>
                                <label class="form-label"> 
                                    Raz�o Social 

                                </label>
                                <div class="form-input">
                                    <input type="text" name="razao" placeholder="Raz�o Social" required >
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    Telefone

                                </label>
                                <div class="form-input">
                                    <input type="text" name="telefone1" placeholder="Telefone" required >
                                </div>
                                <label class="form-label">
                                    Telefone 2							   
                                </label>
                                <div class="form-input">
                                    <input type="text" name="telefone2" placeholder="Telefone 2 (opcional)" required >
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    E-Mail

                                </label>
                                <div class="form-input">
                                    <input type="email" name="email" placeholder="seunome@examplo.com" required>
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    Senha

                                </label>
                                <div class="form-input">
                                    <input type="password" name="pass" placeholder="Sua senha" required>
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    Confirmar Senha

                                </label>
                                <div class="form-input">
                                    <input type="password" name="passConf" placeholder="Confirmar Senha" required>
                                </div>
                            </li>
                            <li>
                                <%
                                    String sql2 = "select *from segmento";
                                    pstm = con.prepareStatement(sql2);
                                    rs = pstm.executeQuery();
                                    rs.first();
                                %>
                                <label class="form-label">
                                    Segmento                                    
                                </label>
                                <div class="form-input">
                                    <select class="form-dropdown" name="segmento" required>
                                        <%
                                            do {
                                        %>   
                                        <option value="<%= rs.getString("seg_desc")%>"><%out.println(rs.getString("seg_desc")); %> </option>
                                        <% } while (rs.next());
                                        %>
                                    </select>
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    Endere�o
                                    <span class="form-required"> * </span>
                                </label>
                                <div class="form-input add">
                                    <span class="form-sub-label">
                                        <input type="text" name="rua" placeholder=" " required>
                                        <label class="form-sub-label1">Rua</label>
                                    </span>
                                    <span class="form-sub-label">
                                        <input type="text" name="complemento" placeholder=" " required>
                                        <label class="form-sub-label1">Complemento</label>
                                    </span>
                                    <span class="form-sub-label">
                                        <input type="text" name="ref" placeholder=" " required>
                                        <label class="form-sub-label1">Ref</label>
                                    </span>
                                    <span class="form-sub-label">
                                        <input type="text" name="cep" required>
                                        <label class="form-sub-label1">Cep Postal</label>
                                    </span>   
                                    <span class="form-sub-label">
                                        <%
                                            String sql = "select * from bairro";
                                            pstm = con.prepareStatement(sql);
                                            rs = pstm.executeQuery();
                                            rs.first();
                                        %>
                                        <select class="form-dropdown" name="bairro" required>
                                            <%
                                                do {
                                            %>   

                                            <option value="<%= rs.getString("bai_desc")%>"><%out.println(rs.getString("bai_desc")); %> </option>

                                            <% } while (rs.next());
                                            %>
                                        </select>
                                        <label class="form-sub-label1">Bairro</label>
                                    </span>
                                </div>                                                      
                            <li>
                                <div class="input-group">
                                    <span>
                                        <button class="btn-primary" type="submit">Salvar Dados</button>
                                        <button class="btn-primary" type="reset">Cancelar</button>
                                        <button class="btn-primary" type="button" onclick="document.location.href='tipoCad.jsp'">Voltar</button>  
                                    </span> 
                                </div>                                
                            </li>
                        </ul>					
                    </form>	
                </div>
            </div>
        </div>
        <!-- js -->
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script>
            $('ul.dropdown-menu li').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            });
        </script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
    </body>

</html>
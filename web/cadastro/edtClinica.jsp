<%@page import="model.Dao.DaoUsuario"%>
<%@page import="model.Dao.DaoFones"%>
<%@page import="model.Dao.DaoClinica"%>
<%@page import="model.bean.BeanClinica"%>
<%@page import="model.bean.BeanSegmento"%>
<%@page import="model.bean.BeanFones"%>
<%@page import="model.bean.BeanUsuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Dao.BaseDao"%>
<%@page import="java.util.List"%>
<%@page import="model.bean.BeanBairro"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.Connection"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>

        <title>Edt - Clinica </title>
        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript">
            addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
            window.scrollTo(0, 1);
            }
        </script>
        <!--//tags -->
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/font-awesome.css" rel="stylesheet">
        <!-- //for bootstrap working -->
        <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
    </head>
    <body>	 
        <%
            Connection con;
            PreparedStatement pstm;
            ResultSet rs;
            con = BaseDao.getConnection();
            BeanClinica clinica = new BeanClinica();
            BeanBairro bairro = new BeanBairro();
            BeanUsuario usuario = new BeanUsuario();
            int codigo = (int) request.getSession().getAttribute("user_key");
            usuario = new DaoUsuario().buscarporcodigo(codigo);
            BeanFones fones = new BeanFones();
            BeanSegmento sg = new BeanSegmento();
            String email = (String) request.getSession().getAttribute("user");
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT c.clin_cod, fc.fon_clincod, u.usu_cod ");
            sb.append("FROM clinica c INNER JOIN ");
            sb.append("fonesclin fc ON fc.fon_clincod = c.clin_cod ");
            sb.append("INNER JOIN usuario u ON u.usu_cod = c.clin_usucod ");
            sb.append("where u.usu_email = ?");

            pstm = con.prepareStatement(sb.toString());
            pstm.setString(1, email);
            rs = pstm.executeQuery();
            while (rs.next()) {               
                fones = new DaoFones().buscarPorClinica(rs.getInt("fon_clincod"));
                 clinica = new DaoClinica().buscarPorCodigo(rs.getInt("clin_cod"));
            }
        %>
        <p></p>
        <p></p>
        <div class="w3ls-banner">
            <div class="container_1">
                <div class="heading">
                    <h2>Cl�nica</h2>	
                    <p></p>
                </div>
                <div class="agile-form">
                    <form action="../ControllerClinica" method="put">
                        <ul class="field-list">
                            <li>
                                <label class="form-label"> 
                                    Cnpj 
                                </label>
                                <div class="form-input">
                                    <input type="text" readonly="true" value="<%= clinica.getClin_cnpj()%>" name="cnpj" placeholder="Entre com o cnpj" required >
                                </div>
                            </li>
                            <li>
                                <label class="form-label"> 
                                    Raz�o Social 

                                </label>
                                <div class="form-input">
                                    <input type="text" value="<%= clinica.getClin_razao()%>" name="razao" placeholder="Raz�o Social" required >
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    Telefone 1

                                </label>
                                <div class="form-input">
                                    <input type="text" value="<%= fones.getFon_num1()%>" name="telefone1" placeholder="Telefone" required >
                                </div>
                                <label class="form-label">
                                    Telefone 2							   
                                </label>
                                <div class="form-input">
                                    <input type="text" value="<%= fones.getFon_num2()%>"  name="telefone2" placeholder="Telefone 2 (opcional)" required >
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    E-Mail

                                </label>
                                <div class="form-input">
                                    <input type="email" value="<%= usuario.getUsu_email()%>" name="email" placeholder="seunome@examplo.com" required>
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    Senha

                                </label>
                                <div class="form-input">
                                    <input type="password" name="pass" placeholder="Sua senha" required>
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    Confirmar Senha

                                </label>
                                <div class="form-input">
                                    <input type="password" name="passConf" placeholder="Confirmar Senha" required>
                                </div>
                            </li>
                            <li>
                                <%
                                    String sql2 = "select * from segmento";
                                    pstm = con.prepareStatement(sql2);
                                    rs = pstm.executeQuery();
                                    rs.first();
                                %>
                                <label class="form-label">
                                    Segmento                                    
                                </label>
                                <div class="form-input">
                                    <select class="form-dropdown" name="segmento" required>
                                        <%
                                            do {
                                        %>   
                                        <% if (rs.getString("seg_desc").equalsIgnoreCase(clinica.getClin_segcod().getSeg_desc())) {%>
                                        <option value="<%= rs.getString("seg_cod") %>" selected="true"><% out.print(rs.getString("seg_desc")); %></option>
                                        <%} else {%>
                                        <option value="<%= rs.getString("seg_cod")%>"><% out.print(rs.getString("seg_desc")); %> </option>
                                        <%}%>
                                        <%
                                            } while (rs.next());
                                        %>
                                    </select>
                                </div>
                            </li>
                            <li> 
                                <label class="form-label">
                                    Endere�o
                                    <span class="form-required"> * </span>
                                </label>
                                <div class="form-input add">
                                    <span class="form-sub-label">
                                        <label class="form-sub-label1">Rua</label>
                                        <input type="text" value="<%= usuario.getUsu_endcod().getEnd_rua()%>" name="rua" placeholder=" " required>

                                    </span>
                                    <span class="form-sub-label">
                                        <label class="form-sub-label1">Complemento</label>
                                        <input type="text" value="<%= usuario.getUsu_endcod().getEnd_complemento()%>" name="complemento" placeholder=" " required>

                                    </span>
                                    <span class="form-sub-label">
                                        <label class="form-sub-label1">Ref</label>
                                        <input value="<%= usuario.getUsu_endcod().getEnd_ref()%>" type="text" name="ref" placeholder=" " required>

                                    </span>
                                    <span class="form-sub-label">
                                        <label class="form-sub-label1">Cep Postal</label>
                                        <input value="<%= usuario.getUsu_endcod().getEnd_cep()%>" type="text" name="cep" required>
                                    </span>   
                                    <span class="form-sub-label">
                                        <%
                                            String sql = "select * from bairro";
                                            pstm = con.prepareStatement(sql);
                                            rs = pstm.executeQuery();
                                            rs.first();
                                        %>
                                        <select class="form-dropdown" name="bairro" required>
                                            <%
                                                do {
                                            %>   
                                            <% if (rs.getString("bai_desc").equalsIgnoreCase(usuario.getUsu_endcod().getEnd_baicod().getBai_desc())) {%>
                                            <option value="<%= rs.getString("bai_cod")%>" selected="true"><%out.println(rs.getString("bai_desc"));%> </option>
                                            <% } else {%>
                                            <option value="<%= rs.getString("bai_cod")%>"><%out.println(rs.getString("bai_desc")); %> </option>
                                            <% } %>
                                            <% } while (rs.next());
                                            %>
                                        </select>
                                        <label class="form-sub-label1">Bairro</label>
                                    </span>
                                </div>                                                      
                            <li>
                                <div class="input-group">
                                    <span>
                                        <input type="hidden" value="<%= usuario.getUsu_cod() %>" name="usuario_id"/>
                                        <input type="hidden" value="edtClinica" name="page"/>
                                        <input type="hidden" value="<%= clinica.getClin_cod() %>" name="clinica_id"/>
                                        <input type="hidden" value="<%= fones.getFon_cod() %>" name="fone_id"/>                                        
                                        <button class="btn-primary" type="submit">Salvar Dados</button>
                                        <button class="btn-primary" type="submit">Cancelar</button>                                                
                                    </span> 
                                </div>                                
                            </li>
                        </ul>
                    </form>	
                </div>
            </div>
        </div>
        <!-- js -->
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script>
            $('ul.dropdown-menu li').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            });
        </script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
    </body>

</html>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<head>
	<title>Cadastro Paciente</title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="../css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
</head>

<body>
    <p></p>
    <p></p>
    <div class="w3ls-banner">
		<div class="container_1">
			<div class="heading">
				<h2>Paciente</h2>
				<p></p>
			</div>
			<div class="agile-form">
				<form action="#" method="post">
					<ul class="field-list">
					<li>
							<label class="form-label"> 
								Cpf 
								<span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<input type="text" name="cpf" placeholder="Cpf" required >
							</div>
						</li>	
                                            <li>
							<label class="form-label"> 
								Nome 
								<span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<input type="text" name="nomePac" placeholder="Nome" required >
							</div>
						</li>                                                
						<li>
							<label class="form-label">
							   Sexo
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<select class="form-dropdown" name="sexo" required>
									<option value="">&nbsp;</option>
									<option value="masculino"> Masculino </option>
									<option value="feminino"> Feminino </option>									
								</select>
							</div>
                                                </li>
						<li>
						<li> 
							<label class="form-label">
							   Telefone
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<input type="text" name="telefone" placeholder="telefone" required >
							</div>
						</li>
						<li>
							<label class="form-label">
							   Data de Nascimento
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input dob">
								<span class="form-sub-label">
									<select name="dia" class="day">
										<option>&nbsp;</option>
										<option value="1"> 1 </option>
										<option value="2"> 2 </option>
										<option value="3"> 3 </option>
										<option value="4"> 4 </option>
										<option value="5"> 5 </option>
										<option value="6"> 6 </option>
										<option value="7"> 7 </option>
										<option value="8"> 8 </option>
										<option value="9"> 9 </option>
										<option value="10"> 10 </option>
										<option value="11"> 11 </option>
										<option value="12"> 12 </option>
										<option value="13"> 13 </option>
										<option value="14"> 14 </option>
										<option value="15"> 15 </option>
										<option value="16"> 16 </option>
										<option value="17"> 17 </option>
										<option value="18"> 18 </option>
										<option value="19"> 19 </option>
										<option value="20"> 20 </option>
										<option value="21"> 21 </option>
										<option value="22"> 22 </option>
										<option value="23"> 23 </option>
										<option value="24"> 24 </option>
										<option value="25"> 25 </option>
										<option value="26"> 26 </option>
										<option value="27"> 27 </option>
										<option value="28"> 28 </option>
										<option value="29"> 29 </option>
										<option value="30"> 30 </option>
										<option value="31"> 31 </option>
									</select>
									<label class="form-sub-label1"> Dia </label>
								</span>
								<span class="form-sub-label">
									<select name="mes">
										<option>&nbsp;</option>
										<option value="January"> Janeiro </option>
										<option value="February"> Fevereiro </option>
										<option value="March"> Março </option>
										<option value="April"> Abril </option>
										<option value="May"> Maio </option>
										<option value="June"> Junho </option>
										<option value="July"> Julho </option>
										<option value="August"> Agosto </option>
										<option value="September"> Setembro </option>
										<option value="October"> Outubro </option>
										<option value="November"> Novembro </option>
										<option value="December"> Dezembro </option>
									 </select>
									<label class="form-sub-label1"> Mês </label>
								</span>
								<span class="form-sub-label">
									<input type="text" class="year" name="ano" placeholder=" Ano" required>
									<label class="form-sub-label1"> Ano </label>
								</span>
							</div>
						</li>						
                                                <li> 
							<label class="form-label">
							   Endereço
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input add">
								<span class="form-sub-label">
									<input type="text" name="street_address" placeholder=" " required>
									<label class="form-sub-label1"> Rua </label>
								</span>
								<span class="form-sub-label">
									<input type="text" name="street_address2" placeholder=" " required>
									<label class="form-sub-label1"> Complemento </label>
								</span>
								<span class="form-sub-label">
									<input type="text" name="city" placeholder=" " required>
									<label class="form-sub-label1">Referência </label>
								</span>
								<span class="form-sub-label">
									<input type="text" name="state" placeholder=" " required>
									<label class="form-sub-label1"> Cep Postal </label>
								</span>								
							</div>
						</li>
						<li> 
							<label class="form-label">
							   E-mail
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<input type="email" name="email" placeholder="seunome@exemplo.com" required>
							</div>
						</li>
                                                <li> 
							<label class="form-label">
							  Senha
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<input type="pass" name="pass" placeholder="Sua senha" required>
							</div>
						</li>
                                                <li> 
							<label class="form-label">
							 Confirmar Senha
							   <span class="form-required"> * </span>
							</label>
							<div class="form-input">
								<input type="passConf" name="passConf" placeholder="Confirmar Senha" required>
							</div>
						</li>
                                                <li>
                                        <div class="input-group">
                                            <span>
                                                <button class="btn-primary" type="submit">Salvar Dados</button>
                                                <button class="btn-primary" type="submit">Cancelar</button>                                                
                                            </span> 
                                        </div>
                                    </li>
                                        </ul>					
				</form>	
			</div>
		</div>	
    </div>
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>

</html>
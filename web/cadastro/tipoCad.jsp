<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Tipo Cadastro </title>
        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="New Clinic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript">
            addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
            window.scrollTo(0, 1);
            }
        </script>
        <!--//tags -->
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/appointment_style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/font-awesome.css" rel="stylesheet">
        <!-- //for bootstrap working -->
        <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
    </head>

    <body>	
        <div class="top_menu_w3layouts">
            <div class="container">			
                <div class="header_right">
                    <ul class="forms_right">
                        <li><a href="../login.jsp"> Voltar</a> </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="container_1">
            <div class="heading">
                <h2>Tipo de Cadastro</h2>
                <p>Selecione se você é um paciente ou uma clínica/consultorio!</p>
            </div>
            <div class="agile-form">                
                <ul class="field-list">						
                    <li class="last-type"> 
                        <label class="form-label1">
                            Selecione de acordo com a opção que deseja para prosseguir!
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input2">
                            <input type="radio" checked="true" name="tipo" value="clinica"/>
                            <label class="type-of-test">Clínica/Consultorio</label>
                            <input type="radio" name="tipo" value="paciente"/>
                            <label class="type-of-test">Paciente</label>								
                        </div>
                    </li>
                </ul>
                <input type="submit" value="Avançar"  onClick="check_sex();">               
            </div>
        </div>	
        <!-- //bootstrap-modal-pop-up --> 
        <!-- js -->
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script>

                    function check_sex() {
                        var radioBtns = document.getElementsByName("tipo");
                        for (i = 0; i < radioBtns.length; i++) {
                            if (radioBtns[i].checked) {
                                if (radioBtns[i].value === 'clinica')
                                    location.href = "cadClinica.jsp";
                                else
                                    location.href = "cadPaciente.jsp";
                            }
                        }
                    }
        </script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>       
        <script type="text/javascript" src="js/bootstrap.js"></script>
    </body>

</html>